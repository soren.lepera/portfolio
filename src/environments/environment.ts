// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

const firebaseConfig = {
  apiKey: 'AIzaSyAvs6fw_QMBrJUOgXE-Kp7pQ_h4nIfygb0',
  authDomain: 'my-project-1521059798559.firebaseapp.com',
  databaseURL: 'https://my-project-1521059798559.firebaseio.com',
  projectId: 'my-project-1521059798559',
  storageBucket: 'my-project-1521059798559.appspot.com',
  messagingSenderId: '757033699798',
  appId: '1:757033699798:web:61741e8d1cac8fbe877c77',
  measurementId: 'G-8JBS5L4VQ6'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
