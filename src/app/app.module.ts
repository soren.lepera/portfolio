import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BlogPostComponent } from './blog-post/blog-post.component';
import { AppRoutingModule } from './app-routing.module';
import { IndexComponent } from './index/index.component';
import { PostListComponent } from './blog-post/post-list/post-list.component';
import { PostlistitemComponent } from './blog-post/post-list/postlistitem/postlistitem.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { ContactComponent } from './contact/contact.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AgmCoreModule } from '@agm/core';
import { ProjectsComponent } from './projects/projects.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';

const firebaseConfig = {
  apiKey: 'AIzaSyAvs6fw_QMBrJUOgXE-Kp7pQ_h4nIfygb0',
  authDomain: 'my-project-1521059798559.firebaseapp.com',
  databaseURL: 'https://my-project-1521059798559.firebaseio.com',
  projectId: 'my-project-1521059798559',
  storageBucket: 'my-project-1521059798559.appspot.com',
  messagingSenderId: '757033699798',
  appId: '1:757033699798:web:61741e8d1cac8fbe877c77',
  measurementId: 'G-8JBS5L4VQ6'
};

@NgModule({
  declarations: [
    AppComponent,
    BlogPostComponent,
    IndexComponent,
    PostListComponent,
    PostlistitemComponent,
    MainNavComponent,
    ContactComponent,
    NotfoundComponent,
    ProjectsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatGridListModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCBMUACMBo8QQiQn_m2UlUcqxDYbl77zm0'
    }),
    FontAwesomeModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
