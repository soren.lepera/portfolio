import { Component, OnInit } from '@angular/core';

export interface Project {
  text: string;
  title: string;
  subtitle: string;
  img: string;
}

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  projects: Project[] = [
    {
      title: 'Web Agency',
      subtitle: 'Projet Openclassrooms',
      img: 'assets/webagency.PNG',
      text: 'One',
    },
    {
      title: 'Web Agency',
      subtitle: 'Projet Openclassrooms',
      img: 'assets/webagency.PNG',
      text: 'One',
    },
    {
      title: 'Web Agency',
      subtitle: 'Projet Openclassrooms',
      img: 'assets/webagency.PNG',
      text: 'One',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
