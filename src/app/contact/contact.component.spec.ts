import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { ContactComponent } from './contact.component';

import { AngularFireModule } from '@angular/fire';

describe('ContactComponent', () => {
  let component: ContactComponent;
  let fixture: ComponentFixture<ContactComponent>;

  const firebaseConfig = {
    apiKey: 'AIzaSyAvs6fw_QMBrJUOgXE-Kp7pQ_h4nIfygb0',
    authDomain: 'my-project-1521059798559.firebaseapp.com',
    databaseURL: 'https://my-project-1521059798559.firebaseio.com',
    projectId: 'my-project-1521059798559',
    storageBucket: 'my-project-1521059798559.appspot.com',
    messagingSenderId: '757033699798',
    appId: '1:757033699798:web:61741e8d1cac8fbe877c77',
    measurementId: 'G-8JBS5L4VQ6'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        AngularFireModule.initializeApp(firebaseConfig),
      ],
      declarations: [ ContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
