import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.scss']
})
export class BlogPostComponent implements OnInit {

  posts = [
    {
      title: 'Hello World',
      content: 'Encore un bout de code pour le bien etre de la tradittion',
      loveIts: 24,
      createdAt: 'Wed Oct 30 2018 16:33:22'
    },
    {
      title: 'Les Closures ',
      content: 'Encore cette histoire qui casse la tete',
      loveIts: 0,
      createdAt: 'Wed Oct 27 2018 16:33:22'
    },
    {
      title: 'Hello Java',
      content: 'Encore un bout de code pour le bien etre de la tradittion',
      loveIts: 12,
      createdAt: 'Wed Oct 12 2018 16:33:22'
    },
    {
      title: 'Hello Python',
      content: 'Encore un bout de code pour le bien etre de la tradittion',
      loveIts: 10,
      createdAt: 'Wed Oct 01 2018 16:33:22'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
