import { Component, OnInit, Input } from '@angular/core';


import { BlogPostComponent } from '../blog-post.component';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  @Input() title: string;
  @Input() content: string;
  @Input() loveIts: number;
  @Input() createdAt: Date;
  @Input() posts: any;

  constructor() { }

  ngOnInit(): void {
  }

}
